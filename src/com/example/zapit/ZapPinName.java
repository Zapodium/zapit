package com.example.zapit;


import java.util.Calendar;

public class ZapPinName {
    private String pinName;
    private String mapName;
    public ZapPinName(String pin, String map){
        pinName = pin;
        mapName = map;
    }

    public String getLongName(){
        return pinName + "#" + mapName;
    }
    public static ZapPinName createDefaultPinName(String mapName){
        return new ZapPinName(getDefaultPinName(), mapName);
    }

    public ZapPinName(String longName) {
        parseLongName(longName);
    }

    private void parseLongName(String longName) {
        String[] parts = longName.split("#");
        if(parts.length == 0) return;
        pinName = parts[0];
        if(parts.length > 1)
            mapName = parts[1];
        else
            mapName = "";
    }

    private static String getDefaultPinName() {
        Calendar c = Calendar.getInstance();
        int seconds = c.get(Calendar.SECOND);
        return c.getTime().toString().substring(11, 19);
    }

    public int getDefaultNameLength() {
        return 8;
    }

    public String getMapName() {
        return mapName;
    }

    public String getPinName() {
        return pinName;
    }
}
