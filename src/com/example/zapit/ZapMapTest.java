package com.example.zapit;

import com.google.android.gms.maps.model.MarkerOptions;
import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintWriter;
import java.util.List;

import static org.junit.Assert.*;

public class ZapMapTest {
    private ZapMap map;
    private FakeIZapStore zapStore;

    @Before
    public void setup() {
        zapStore = new FakeIZapStore();
        map = new ZapMap("test", zapStore);
        BasicConfigurator.configure();
    }

    @Test
    public void testReturnFalseWhenAddedTwice() throws Exception {
        Assert.assertTrue(map.addPin("loc", 50.0, 5.0));
        Assert.assertFalse(map.addPin("loc", 5.5, 5.5));
    }

    @Test
    public void testGetLocationIsSameAsGivenInCreation() throws Exception {
        Assert.assertTrue(map.addPin("loc", 50.0, 5.0));
        assertExist(50.0, 5.0);
    }

    private void assertExist(double expectedLat, double expectedLon) {
        List<MarkerOptions> markers = map.getMarkers();
        for (MarkerOptions m : markers) {
            if (0 == Double.compare(m.getPosition().latitude, expectedLat) &&
                    0 == Double.compare(m.getPosition().longitude, expectedLon))
                return;
        }
        fail(expectedLat + " " + expectedLon + " did not exist");
    }

    @Test
    public void testGetSecondLocationIsSameAsGiven() throws Exception {
        map.addPin("loc", 50.0, 5.0);
        map.addPin("sec", 90.0, 1.0);
        assertExist(90.0, 1.0);
    }

    @Test
    public void testGetThirdLocationIsSameAsGiven() throws Exception {
        map.addPin("loc", 50.0, 5.0);
        map.addPin("sec", 90.0, 1.0);
        map.addPin("3rd", 4.0, 8.0);
        assertExist(4.0, 8.0);
    }

    @Test
    public void testGetSerializedMapWithOnePin() throws Exception {
        map.addPin("loc", 64.170166, 25.384068);
        assertEquals("\t<wpt lon=\"25.384068\" lat=\"64.170166\">\n" +
                "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                "\t\t<name>loc#test</name>\n" +
                "\t\t<sym>Campground</sym>\n" +
                "\t</wpt>\n", map.getPinGpx(0));
    }

    @Test
    public void testGetSerializedMapWithTwoPins() throws Exception {
        map.addPin("loc", 64.170166, 25.384068);
        map.addPin("sec", 44.44, 55.55);
        assertEquals(
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n" +
                        "<gpx\n" +
                        "version=\"1.0\"\n" +
                        "creator=\"zapit\"\n" +
                        "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n" +
                        "xmlns=\"http://www.topografix.com/GPX/1/0\" \n" +
                        "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd\">\n" +
                        "\t<wpt lon=\"25.384068\" lat=\"64.170166\">\n" +
                        "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                        "\t\t<name>loc#test</name>\n" +
                        "\t\t<sym>Campground</sym>\n" +
                        "\t</wpt>\n" +
                        "\t<wpt lon=\"55.55\" lat=\"44.44\">\n" +
                        "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                        "\t\t<name>sec#test</name>\n" +
                        "\t\t<sym>Campground</sym>\n" +
                        "\t</wpt>\n" +
                        "</gpx>"
                , map.getGpx());
    }


    @Test
    public void testGetGpx() throws Exception {
        map.addPin("loc", 64.170166, 25.384068);
        map.addPin("sec", 44.44, 55.55);
        String wpt1 = "<wpt lat=\"64.170166\" lon=\"25.384068\"><name>loc</name></wpt>";
        String wpt2 = "<wpt lat=\"44.44\" lon=\"55.55\"><name>sec</name></wpt>";
        assertWaypointExist("44.44", "55.55", "sec#test");
        assertWaypointExist("64.170166", "25.384068", "loc#test");
    }

    private void assertWaypointExist(String la, String lo, String name) {
        String w = "<wpt lat=\"" + la + "\" lon=\"" + lo + "\"><name>" + name + "</name></wpt>";
        assertTrue(map.getParsedGpx().contains(w));
    }
    /*
    @Test
    public void testNoPinsAfterInvalidGpxGiven() throws Exception {
        String fileName = "file.gpx";
        PrintWriter out = new PrintWriter(fileName);
        out.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n");
        out.close();
        map.importGpx(fileName);
    }*/

    @Test
    public void testGetPinsAfterGpxGiven() throws Exception {
        String fileName = "file.gpx";
        PrintWriter out = new PrintWriter(fileName);
        out.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n" +
                "<gpx\n" +
                "version=\"1.0\"\n" +
                "creator=\"zapit\"\n" +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n" +
                "xmlns=\"http://www.topografix.com/GPX/1/0\" \n" +
                "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd\">\n" +
                "\t<wpt lon=\"25.384068\" lat=\"64.170166\">\n" +
                "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                "\t\t<name>loc</name>\n" +
                "\t\t<sym>Campground</sym>\n" +
                "\t</wpt>\n" +
                "\t<wpt lon=\"55.55\" lat=\"44.44\">\n" +
                "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                "\t\t<name>sec</name>\n" +
                "\t\t<sym>Campground</sym>\n" +
                "\t</wpt>\n" +
                "\t<wpt lon=\"55.99\" lat=\"22.44\">\n" +
                "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                "\t\t<name>3rd</name>\n" +
                "\t\t<sym>Campground</sym>\n" +
                "\t</wpt>\n" +
                "</gpx>\n");
        out.close();
        map.importGpx(fileName);
        assertExist(22.44, 55.99);
        assertExist(64.170166, 25.384068);
        assertExist(44.44, 55.55);
    }

    @Test
    public void testAddMapMembers() throws Exception {
        map.addMember("John");
        assertTrue(map.getMembers().contains("John"));
        map.addMember("Johan");
        assertTrue(map.getMembers().contains("Johan"));
    }

    @Test
    public void testRemoveMapMember() throws Exception {
        map.addMember("John");
        assertTrue(map.getMembers().contains("John"));
        map.removeMember("John");
        assertFalse(map.getMembers().contains("John"));
    }

    @Test
    public void testRemoveNonExistingMapMember() throws Exception {
        map.removeMember("Johan");
    }
    @Test
    public void testAddExistingMapMember() throws Exception {
        map.addMember("John");
        map.addMember("John");
        assertEquals(1, map.getMembers().size());
    }

    @Test
    public void testImportMapAsString() throws Exception {
        String serializedMap = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n" +
                "<gpx\n" +
                "version=\"1.0\"\n" +
                "creator=\"zapit\"\n" +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n" +
                "xmlns=\"http://www.topografix.com/GPX/1/0\" \n" +
                "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd\">\n" +
                "\t<wpt lon=\"25.384068\" lat=\"64.170166\">\n" +
                "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                "\t\t<name>loc</name>\n" +
                "\t\t<sym>Campground</sym>\n" +
                "\t</wpt>\n" +
                "\t<wpt lon=\"55.55\" lat=\"44.44\">\n" +
                "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                "\t\t<name>sec</name>\n" +
                "\t\t<sym>Campground</sym>\n" +
                "\t</wpt>\n" +
                "\t<wpt lon=\"55.99\" lat=\"22.44\">\n" +
                "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                "\t\t<name>3rd</name>\n" +
                "\t\t<sym>Campground</sym>\n" +
                "\t</wpt>\n" +
                "</gpx>\n";
        map.importGpxAsString(serializedMap);
        assertExist(22.44, 55.99);
        assertExist(64.170166, 25.384068);
        assertExist(44.44, 55.55);
    }

}
