package com.example.zapit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

public class ZapitActivity extends FragmentActivity {
    public static final String LOCATION_DB_NAME = "LocationDB";
    private static ZapLocationManager zapLocationManager;
    private static Context myApplicationContext;
    private static ZapMapCase zapMapCase;


    private final String ZAP_PARSE_APP_ID = "6uLEHS7YqRIJpyfku6QjFf09mYbMExfJLGfbanY0";
    private final String ZAP_PARSE_USER_KEY = "obv7ViDksjzwJI1RWXdpKgp2FQRtIDBv06AkI53x";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConfigureLog4J.configure();


        InitCloud();

        ZapStore store = new ZapStore();
        myApplicationContext = getApplicationContext();
        zapMapCase = new ZapMapCase(store);
        setContentView(R.layout.main);

        Intent intent = new Intent(ZapitActivity.getAppContext(), LoginActivity.class);
        startActivity(intent);

        zapLocationManager = new ZapLocationManager();
        View basketView = findViewById(R.id.basketView1);
        basketView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NameDialog.RequestNameAndStore(findViewById(R.id.mainlayout), "sports");
            }
        });

        View basketView2 = findViewById(R.id.basketView2);
        basketView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NameDialog.RequestNameAndStore(findViewById(R.id.mainlayout), "public");
            }
        });
        View basketView3 = findViewById(R.id.basketView3);
        basketView3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NameDialog.RequestNameAndStore(findViewById(R.id.mainlayout), "gold");
            }
        });
        View basketView4 = findViewById(R.id.basketView4);
        basketView4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NameDialog.RequestNameAndStore(findViewById(R.id.mainlayout), "songs");
            }
        });
        View basketView5 = findViewById(R.id.basketView5);
        basketView5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NameDialog.RequestNameAndStore(findViewById(R.id.mainlayout), "birds");
            }
        });
        View basketView6 = findViewById(R.id.basketView6);
        basketView6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NameDialog.RequestNameAndStore(findViewById(R.id.mainlayout), "garage");
            }
        });
        findViewById(R.id.mainlayout).setOnTouchListener(new OnSwipeTouchListener(getAppContext()) {
            public void onSwipeTop() {
                NameDialog.RequestNameAndStore(findViewById(R.id.mainlayout), "home");
            }

            public void onSwipeRight() {

            }

            public void onSwipeLeft() {

                Intent intent = new Intent(ZapitActivity.getAppContext(), MapActivity.class);
                startActivity(intent);

            }

            public void onSwipeBottom() {

            }

            public boolean onTouch(View v, MotionEvent event) {
                return this.getGestureDetector().onTouchEvent(event);
            }
        });


    }

    private void InitCloud() {
        Parse.initialize(this, ZAP_PARSE_APP_ID, ZAP_PARSE_USER_KEY);
        InitCloudSecurity();
    }

    private void InitCloudSecurity() {
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        // Optionally enable public read access while disabling public write access.
        // defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);
    }

    /* Request updates at startup */
    @Override
    protected void onResume() {
        super.onResume();
        zapLocationManager.resume();
    }

    /* Remove the locationlistener updates when Activity is paused */
    @Override
    protected void onPause() {
        super.onPause();
        zapLocationManager.pause();
    }

    public static Context getAppContext() {
        return myApplicationContext.getApplicationContext();
    }

    public static ZapLocationManager getZapLocationManager() {
        return zapLocationManager;
    }
    public static ZapMapCase getZapMapCase() {
        return zapMapCase;
    }
}
