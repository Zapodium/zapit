package com.example.zapit;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;


public class MapActivity extends Activity {
    private GoogleMap map;
    private static Context myApplicationContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (status == ConnectionResult.SUCCESS) {
            // what you want to do
            setContentView(R.layout.mapview);
        }

        myApplicationContext = getApplicationContext();
        ZapMapCase mapCase = ZapitActivity.getZapMapCase();
        ArrayList<String> allMaps = mapCase.getMapList();

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                .getMap();
        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker marker) {
                NameDialog.ConfirmAndRemove(findViewById(R.id.map), marker);
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onMarkerDrag(Marker marker) {
                // TODO Auto-generated method stub

            }
        });


        LatLng lastPin = null;
        for(String currentMap: allMaps){
            ArrayList<MarkerOptions> pins = mapCase.getPins(currentMap);
            for (MarkerOptions marker: pins) {
                marker.draggable(true);
                map.addMarker(marker);
                lastPin = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);
            }
        }

        if (lastPin != null)
        {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(lastPin, 15));
            map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
        }

    }
    public static Context getAppContext() {
        return myApplicationContext.getApplicationContext();
    }
}


