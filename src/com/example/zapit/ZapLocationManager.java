package com.example.zapit;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class ZapLocationManager implements LocationListener {

    private LocationManager locationManager;
    private String provider;
    private double currentLatitude;
    private double currentLongitude;

    public ZapLocationManager() {
        setupLocationManager();
    }

    private void setupLocationManager() {
        locationManager = (LocationManager) ZapitActivity.getAppContext().getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the location provider -> use default
        if (locationManager != null) {
            //boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            // check if enabled and if not send user to the GSP settings
            // Better solution would be to display a dialog and suggesting to
            // go to the settings
            /*if (!enabled) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                * TODO: after setting GPS on, location need to be
                retrieved somehow, it keeps as "not available"*
            } */
            ConfigureLog4J.configure();
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            // Initialize the location fields
            if (location != null) {
                System.out.println("Provider " + provider + " has been selected.");
                onLocationChanged(location);
            } else {
            }
        }
    }

    public void resume() {
        if (locationManager != null)
            locationManager.requestLocationUpdates(provider, 400, 1, this);
        updateCurrentLocation();
    }

    public void pause() {
        if (locationManager != null)
            locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        setCurrentLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        updateCurrentLocation();
    }


    @Override
    public void onProviderEnabled(String provider) {
        updateCurrentLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
        updateCurrentLocation();
    }

    private void updateCurrentLocation() {
        if (locationManager != null) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            if (location != null)
                setCurrentLocation(location);
        }
    }

    private void setCurrentLocation(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
    }

    public double getCurrentLatitude() {
        return currentLatitude;
    }

    public double getCurrentLongitude() {
        return currentLongitude;
    }

    public String getLongitudeString() {
        return Double.toString(currentLongitude);
    }

    public String getLatitudeString() {
        return Double.toString(currentLatitude);
    }
}
