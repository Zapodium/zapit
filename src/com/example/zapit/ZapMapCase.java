package com.example.zapit;

import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Map;

public class ZapMapCase implements IZapUser, IZapMapCase {
    private String accountName = "";
    private ArrayList<ZapMap> maps;
    private IZapStore zapStore;

    public ZapMapCase(IZapStore store) {
        maps = new ArrayList<ZapMap>();
        zapStore = store;
        Map<String, String> allMaps = zapStore.getMaps();
        for (Map.Entry<String, String> entry : allMaps.entrySet()) {
            createMap(entry.getKey());
            ZapMap newMap = findMap(entry.getKey());
            newMap.importGpxAsString(entry.getValue().toString());
        }
    }

    @Override
    public boolean createAccount(String a) {
        if (accountName != "") return false;
        accountName = a;
        return true;
    }

    @Override
    public boolean createMap(String name) {
        if (mapAlreadyExist(name)) return false;
        maps.add(new ZapMap(name, zapStore));
        return true;
    }

    private boolean mapAlreadyExist(String n) {
        for (ZapMap z : maps) if (z.getName().equals(n)) return true;
        return false;
    }

    @Override
    public ArrayList<String> getMapList() {
        ArrayList mapNames = new ArrayList<String>();
        for (ZapMap z : maps) mapNames.add(z.getName());
        return mapNames;
    }

    @Override
    public boolean addPin(String mapName, String pinName, double lat, double lon) {
        ZapMap z = findMap(mapName);
        if(z != null) return z.addPin(pinName, lat, lon);
        return false;
    }

    private ZapMap findMap(String map){
        for (ZapMap z : maps) {
            if (z.getName().equals(map))
                return z;
        }
        return null;
    }
    @Override
    public ArrayList<MarkerOptions> getPins(String mapName) {
        ZapMap z = findMap(mapName);
        if(z != null) return z.getMarkers();
        return null;
    }

    @Override
    public boolean removeMap(String s) {
        int i = 0;
        for (ZapMap z: maps) {
            if(z.getName().equals(s)){
                maps.remove(i);
                zapStore.removeMap(s);
                return true;
            }
            i++;
        }
        return false;
    }

    @Override
    public void removePin(ZapPinName zapPinName) {
        ZapMap z = findMap(zapPinName.getMapName());
        if(z != null) z.removePin(zapPinName);
    }
}
