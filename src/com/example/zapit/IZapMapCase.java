package com.example.zapit;

import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public interface IZapMapCase {
    public abstract boolean createMap(String n);

    public abstract ArrayList<String> getMapList();

    public abstract boolean addPin(String mapName, String pinName, double lat, double lon);

    public abstract ArrayList<MarkerOptions> getPins(String mapName);

    public abstract boolean removeMap(String s);

    public abstract void removePin(ZapPinName pinName);
}
