package com.example.zapit;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import static com.example.zapit.ZapPinName.createDefaultPinName;

class NameDialog {
    private static Marker markerToRemove;
    private static LatLng original;

    public static void ConfirmAndRemove(View v, Marker marker) {
        markerToRemove = marker;
        original = marker.getPosition(); //does not work. need to keep original latlng in own data field.
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        ZapPinName zapName = new ZapPinName(marker.getTitle());
        builder.setTitle("Remove " + zapName.getLongName() + " ?");

        final EditText input = new EditText(v.getContext());

        input.setText(zapName.getLongName());
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                ZapPinName pinName = new ZapPinName(input.getText().toString());
                removePin(pinName);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                markerToRemove.setPosition(original);
                dialog.cancel();
            }
        });
        builder.show();
    }
    public static void RequestNameAndStore(View v, String mapName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setTitle("Add current location");

        final EditText input = new EditText(v.getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE);
        ZapPinName zapName = new ZapPinName(mapName);
        input.setText(createDefaultPinName(mapName).getLongName());
        input.setSelection(0, zapName.getDefaultNameLength());
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                ZapPinName pinName = new ZapPinName(input.getText().toString());
                storeCurrentLocation(pinName.getMapName(), pinName.getPinName());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        SetKeyboardVisible(builder);
    }

    private static void SetKeyboardVisible(AlertDialog.Builder builder) {
        AlertDialog alertToShow = builder.create();
        alertToShow.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        alertToShow.show();
    }

    private static void storeCurrentLocation(String mapName, String locationName) {
        ZapMapCase mapCase = ZapitActivity.getZapMapCase();
        if(!mapCase.getMapList().contains(mapName)) mapCase.createMap(mapName);
        mapCase.addPin(mapName, locationName, ZapitActivity.getZapLocationManager().getCurrentLatitude(),
                ZapitActivity.getZapLocationManager().getCurrentLongitude());
    }

    private static void removePin(ZapPinName pin) {
        ZapMapCase mapCase = ZapitActivity.getZapMapCase();
        mapCase.removePin(pin);
        markerToRemove.remove();
    }
}
