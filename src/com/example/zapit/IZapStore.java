package com.example.zapit;


import java.util.Map;

public interface IZapStore {
    public abstract void removeMap(String s);

    public abstract void storeMap(String n, String s);

    public abstract Map<String, String> getMaps();
}
