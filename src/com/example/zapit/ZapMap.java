package com.example.zapit;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import org.alternativevision.gpx.GPXParser;
import org.alternativevision.gpx.beans.GPX;
import org.alternativevision.gpx.beans.Waypoint;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;

public class ZapMap {
    private String mapName;
    private ArrayList<MarkerOptions> markers;
    private final String gpxCreator = "zapit";
    private ArrayList<String> members;
    private IZapStore zapStore;

    public ZapMap(String n, IZapStore store) {
        mapName = n;
        markers = new ArrayList<MarkerOptions>();
        members = new ArrayList<String>();
        zapStore = store;
        zapStore.storeMap(n, "");
    }

    public boolean addPin(String name, double lat, double lon) {
        if (name == "") return false;
        ZapPinName zapName = new ZapPinName(name, mapName);
        for (MarkerOptions m : markers) if (m.getTitle().equals(zapName.getLongName())) return false;
        LatLng location = new LatLng(lat, lon);
        markers.add(new MarkerOptions().position(location).alpha((float) 0.5).title(zapName.getLongName()));
        zapStore.storeMap(mapName, getParsedGpx());
        return true;
    }

    public ArrayList<MarkerOptions> getMarkers() {
        return markers;
    }

    private String getLonLatLine(int i) {
        return "\t<wpt lon=\"" + markers.get(i).getPosition().longitude +
                "\" lat=\"" + markers.get(i).getPosition().latitude + "\">\n";
    }

    private String getTimeLine() {
        return "\t\t<time>2012-10-23T15:57:21Z</time>\n";
    }

    public String getGpx() {
        return getGpxHeader() +
                getPinGpx(0) +
                getPinGpx(1) +
                getGpxEndMark();
    }


    public String getPinGpx(int i) {
        return
                getLonLatLine(i) +
                        getTimeLine() +
                        getNameLine(i) +
                        getSymbol() +
                        getEndMark();
    }

    private String getGpxEndMark() {
        return "</gpx>";
    }

    private String getGpxHeader() {
        return
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n" +
                        "<gpx\n" +
                        "version=\"1.0\"\n" +
                        "creator=\"" + gpxCreator + "\"\n" +
                        "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n" +
                        "xmlns=\"http://www.topografix.com/GPX/1/0\" \n" +
                        "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd\">\n";
    }

    private String getEndMark() {
        return "\t</wpt>\n";
    }

    private String getSymbol() {
        return "\t\t<sym>Campground</sym>\n";
    }

    private String getNameLine(int i) {
        return "\t\t<name>" + markers.get(i).getTitle() + "</name>\n";
    }

    public void importGpx(String fileName) {
        try {
            GPXParser parser = new GPXParser();
            FileInputStream input = new FileInputStream(fileName);
            GPX gpx = parser.parseGPX(input);
            HashSet<Waypoint> wayPoints = gpx.getWaypoints();
            for (Waypoint p : wayPoints)
                addPin(p.getName(), Double.parseDouble(p.getLatitude().toString()), Double.parseDouble(p.getLongitude().toString()));
        } catch (IOException e) {
            // Log.d("GPXParser", "IOException caught: " + e.getMessage());
        } catch (SAXException s) {
            // Log.d("GPXParser", "SAXException caught: " + s.toString());
        } catch (ParserConfigurationException p) {
            // Log.d("GPXParser", "ParserConfigurationException caught: " + p.toString());
        }
    }

    public String getParsedGpx() {
        String xml = "";
        try {
            GPXParser parser = new GPXParser();
            GPX gpx = new GPX();
            for (MarkerOptions m : markers) {
                Waypoint w = new Waypoint();
                w.setLatitude(m.getPosition().latitude);
                w.setLongitude(m.getPosition().longitude);
                w.setName(m.getTitle());
                gpx.addWaypoint(w);
            }
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            parser.writeGPX(gpx, b);
            xml = b.toString();
            b.close();
        } catch (IOException e) {
            //          Log.d("GPXParser", "IOException caught: " + e.getMessage());
        } catch (ParserConfigurationException p) {
            //       Log.d("GPXParser", "ParserConfigurationException caught: " + p.toString());
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return xml;
    }

    public String getName() {
        return mapName;
    }

    public void addMember(String member) {
        if(members.contains(member)) return;
        members.add(member);
    }

    public ArrayList<String> getMembers() {
        return members;
    }

    public void removeMember(String member) {
        members.remove(member);
    }

    public void importGpxAsString(String serializedMap){
        if(serializedMap == "") return;
        InputStream stream = null;
        try {
            stream = new ByteArrayInputStream(serializedMap.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        GPXParser parser = new GPXParser();
        GPX gpx = null;
        try {
            gpx = parser.parseGPX(stream);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(gpx == null) return;
        HashSet<Waypoint> wayPoints = gpx.getWaypoints();
        if(wayPoints == null) return;
        for (Waypoint p : wayPoints){
            ZapPinName pin = new ZapPinName(p.getName());
            addPin(pin.getPinName(), Double.parseDouble(p.getLatitude().toString()), Double.parseDouble(p.getLongitude().toString()));
        }

    }

    public void removePin(ZapPinName pinName) {
        for(MarkerOptions m : markers){
            String longName = pinName.getLongName();
            if(longName.equals(m.getTitle())){
                markers.remove(m);
                zapStore.storeMap(pinName.getMapName(), getParsedGpx());
                return;
            }
        }
    }
}
