package com.example.zapit;

import java.util.HashMap;
import java.util.Map;

public class FakeIZapStore implements IZapStore {
    private String storedMapString;
    private String storedMapName;
    private boolean isRemoved;
    private String serializedTestMap;

    public FakeIZapStore(){
        isRemoved = false;
        storedMapName = "";
        storedMapString = "";
        serializedTestMap = "";
    }

    @Override
    public void removeMap(String s) {
        isRemoved = true;
    }

    @Override
    public void storeMap(String n, String s) {
        storedMapName = n;
        storedMapString = s;
    }

    @Override
    public Map<String, String> getMaps() {
        Map<String, String> outcome = new HashMap<String, String>();
        if(serializedTestMap != "")
            outcome.put("testserialized", serializedTestMap);
        return outcome;
    }

    public String getStoredMap() {
        return storedMapString;
    }

    public String getStoredMapName() {
        return storedMapName;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public void setStoredTestMap(String s) {
        serializedTestMap = s;
    }
}
