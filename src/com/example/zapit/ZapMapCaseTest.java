package com.example.zapit;

import com.google.android.gms.maps.model.MarkerOptions;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.fail;

public class ZapMapCaseTest {
    ZapMapCase zap;
    FakeIZapStore fakeZapStore;

    @Before
    public void setUp() throws Exception {
        fakeZapStore = new FakeIZapStore();
        zap = new ZapMapCase(fakeZapStore);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testOnlyOneAccountCanBeCreated() throws Exception {
        Assert.assertTrue(zap.createAccount("acc"));
        Assert.assertFalse(zap.createAccount("acc2"));
    }

    @Test
    public void testMapCreateTwiceFails() throws Exception {
        Assert.assertTrue(zap.createMap("map one"));
        Assert.assertFalse(zap.createMap("map one"));
        Assert.assertTrue(zap.createMap("map two"));
    }
    @Test
    public void testMapCreateAndRemove() throws Exception {
        Assert.assertTrue(zap.createMap("another map"));
        Assert.assertTrue(zap.createMap("map one"));
        Assert.assertTrue(zap.removeMap("map one"));
        Assert.assertFalse(zap.removeMap("map one"));
        Assert.assertTrue(zap.createMap("map one"));
        Assert.assertTrue(zap.removeMap("another map"));
        Assert.assertEquals(1, zap.getMapList().size());
    }

    @Test
    public void testMapGetCreatedMaps() throws Exception {
        ArrayList<String> maps = new ArrayList<String>();
        maps.add("one");
        maps.add("two");
        maps.add("three");
        for (String s : maps) zap.createMap(s);
        ArrayList<String> receivedMaps = zap.getMapList();
        Assert.assertTrue(maps.equals(receivedMaps));
    }

    @Test
    public void testMapAddPinFailures() throws Exception {
        zap.createMap("one");
        Assert.assertFalse(zap.addPin("one", "", 0.0, 0.0));
        Assert.assertTrue(zap.addPin("one", "pin_a", 0.0, 0.0));
        Assert.assertFalse(zap.addPin("one", "pin_a", 0.0, 0.0));
    }

    private void assertExist(String name, double expectedLat, double expectedLon, ArrayList<MarkerOptions> pins)
    {
        for(MarkerOptions m: pins)
            if(m.getTitle().equals(name) &&
                    0 == Double.compare(m.getPosition().latitude, expectedLat) &&
                    0 == Double.compare(m.getPosition().longitude, expectedLon)
                    ) return;
        fail(name);
    }
    @Test
    public void testMapAddAndGetPins() throws Exception {
        zap.createMap("one");
        Assert.assertTrue(zap.addPin("one", "pin", 1.0, 2.0));
        Assert.assertTrue(zap.addPin("one", "pin2", 3.0, 4.0));
        Assert.assertTrue(zap.addPin("one", "pin3", 5.0, 6.0));
        ArrayList<MarkerOptions> pins = zap.getPins("one");
        assertExist("pin#one", 1.0, 2.0, pins);
        assertExist("pin2#one", 3.0, 4.0, pins);
        assertExist("pin3#one", 5.0, 6.0, pins);
    }

    @Test
    public void testRemovePin() throws Exception {
        zap.createMap("one");
        Assert.assertTrue(zap.addPin("one", "pin", 1.0, 2.0));
        Assert.assertTrue(zap.addPin("one", "pin2", 3.0, 4.0));
        Assert.assertTrue(zap.addPin("one", "pin3", 5.0, 6.0));
        assertExist("pin2#one", 3.0, 4.0, zap.getPins("one"));
        ZapPinName pin = new ZapPinName("pin2", "one");
        zap.removePin(pin);
        assertDoesNotExist(pin, 3.0, 4.0, zap.getPins("one"));
    }

    private void assertDoesNotExist(ZapPinName name, double expectedLat, double expectedLon, ArrayList<MarkerOptions> pins) {
        for(MarkerOptions m: pins)
            if(m.getTitle().equals(name.getLongName()))
                fail(name.getLongName());
    }

    @Test
    public void testStoreMaps() throws Exception {
        zap.createMap("one");
        zap.createMap("two");
        zap.addPin("one", "pin", 1.0, 2.0);
        Assert.assertTrue(fakeZapStore.getStoredMap().contains("pin"));
        Assert.assertTrue(fakeZapStore.getStoredMapName().equals("one"));
        zap.addPin("one", "pin2", 1.0, 2.0);
        Assert.assertTrue(fakeZapStore.getStoredMap().contains("pin2"));
        System.out.print(fakeZapStore.getStoredMap());
        Assert.assertTrue(fakeZapStore.getStoredMapName().equals("one"));
        zap.addPin("two", "pin2", 1.0, 2.0);
        Assert.assertTrue(fakeZapStore.getStoredMap().contains("pin2"));
        System.out.print(fakeZapStore.getStoredMap());
        Assert.assertTrue(fakeZapStore.getStoredMapName().equals("two"));
    }

    @Test
    public void testCreateAndRemoveMaps() throws Exception {
        Assert.assertEquals(fakeZapStore.getStoredMap(), "");
        zap.createMap("one");
        Assert.assertEquals(fakeZapStore.getStoredMap(), "");
        Assert.assertEquals(fakeZapStore.getStoredMapName(), "one");
        zap.removeMap("one");
        Assert.assertTrue(fakeZapStore.isRemoved());
    }
    @Test
    public void testImportExistingMap() throws Exception {

        FakeIZapStore fakeZapStore2 = new FakeIZapStore();
        fakeZapStore2.setStoredTestMap("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n" +
                "<gpx\n" +
                "version=\"1.0\"\n" +
                "creator=\"zapit\"\n" +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n" +
                "xmlns=\"http://www.topografix.com/GPX/1/0\" \n" +
                "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd\">\n" +
                "\t<wpt lon=\"25.384068\" lat=\"64.170166\">\n" +
                "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                "\t\t<name>loc</name>\n" +
                "\t\t<sym>Campground</sym>\n" +
                "\t</wpt>\n" +
                "\t<wpt lon=\"55.55\" lat=\"44.44\">\n" +
                "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                "\t\t<name>sec</name>\n" +
                "\t\t<sym>Campground</sym>\n" +
                "\t</wpt>\n" +
                "\t<wpt lon=\"55.99\" lat=\"22.44\">\n" +
                "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                "\t\t<name>3rd</name>\n" +
                "\t\t<sym>Campground</sym>\n" +
                "\t</wpt>\n" +
                "\t<wpt lon=\"55.99\" lat=\"22.44\">\n" +
                "\t\t<time>2012-10-23T15:57:21Z</time>\n" +
                "\t\t<name>4rd</name>\n" +
                "\t\t<sym>Campground</sym>\n" +
                "\t</wpt>\n" +
                "</gpx>\n");
        ZapMapCase zapMapCase2 = new ZapMapCase(fakeZapStore2);
        Assert.assertEquals(4, zapMapCase2.getPins("testserialized").size());
    }
}
