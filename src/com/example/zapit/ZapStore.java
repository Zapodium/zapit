package com.example.zapit;

import android.content.SharedPreferences;

import java.util.Map;

public class ZapStore implements IZapStore{
    @Override
    public void removeMap(String mapName) {
        SharedPreferences locationDb = ZapitActivity.getAppContext().getSharedPreferences(ZapitActivity.LOCATION_DB_NAME, 0);
        SharedPreferences.Editor editor = locationDb.edit();
        editor.putString(mapName, "");
        editor.commit();

  /*      ParseObject testObject = new ParseObject("Maps");
        testObject.put(mapName, "");
        testObject.saveInBackground();
    */}

    @Override
    public void storeMap(String mapName, String serializedMap) {
        SharedPreferences locationDb = ZapitActivity.getAppContext().getSharedPreferences(ZapitActivity.LOCATION_DB_NAME, 0);
        SharedPreferences.Editor editor = locationDb.edit();
        editor.putString(mapName, serializedMap);
        editor.commit();

/*
        ParseObject testObject = new ParseObject("Maps");
        testObject.put(mapName, serializedMap);
        testObject.saveInBackground();
*/    }

    @Override
    public Map<String, String> getMaps() {
        SharedPreferences mapDb = ZapitActivity.getAppContext().getSharedPreferences(ZapitActivity.LOCATION_DB_NAME, 0);
        return (Map<String, String>) mapDb.getAll();

       /* ParseQuery<ParseObject> query = ParseQuery.getQuery("GameScore");
        query.getInBackground("xWMyZ4YEGZ", new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    // object will be your game score
                } else {
                    // something went wrong
                }
            }
        });    }*/
    }
}
